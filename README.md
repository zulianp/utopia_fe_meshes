Meshes have to follow the naming convention:
	
	<descriptive name>-<element type>-<number of elements>.<extension>

For example:

	a cube with one hexahedral element would be
	cube-hex-1.e
	
Each mesh has to be accompagnied by a description file where blocks, side-sets, and node-sets are described.

For example:
The cube-hex-1.e mesh should come with cube-hex-1.txt file.

The content of the file would look something like

	# top, bottom
	side-sets: 0, 1

	# cube
	blocks: 0


	